package main

//BinarySearchHelper contains the main logic for iterative function : It returns the SearchValue or -1
func BinarySearchHelper(data []int, SearchValue int, LeftPointer int, RightPointer int) int {

	for LeftPointer <= RightPointer {

		MiddleValue := (LeftPointer + RightPointer) / 2

		if data[MiddleValue] == SearchValue {
			return SearchValue
		} else if SearchValue > data[MiddleValue] {
			LeftPointer = MiddleValue + 1
		} else {
			RightPointer = MiddleValue - 1
		}
	}
	return -1
}

func main() {

	data := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 100}
	value := BinarySearchHelper(data, 4, 0, len(data)-1)
	println(value)

}
